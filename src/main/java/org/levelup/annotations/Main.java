package org.levelup.annotations;

public class Main {

    public static void main(String[] args) throws IllegalAccessException {
        RussianRoulette roulette = new RussianRoulette();

        System.out.println();

        RandomIntAnnotationProcessor.setField(roulette);
        System.out.println("In the drum of a revolver bullet was at " + roulette.getNumber() + " position.");
        roulette.guess(4);
    }
}
