package org.levelup.annotations;

import java.lang.reflect.Field;
import java.util.Random;

public class RandomIntAnnotationProcessor {

    public static void setField (Object object) throws IllegalAccessException {

        //1. Получаем объект класса Class
        //2. Находим его поля
        //3. Находим аннотации RandomInt
        //4. Устанавливаем значение

        // object - String -> Class<String>
        // object - Phone -> Class<Phone>
        // object - Shape -> Class<Shape>
        Class<?> objectClass = object.getClass();

        Field[] fields = objectClass.getDeclaredFields();

        for (Field field: fields) {
            System.out.println("\nNext field:");
            // Class<RandomInt>
            System.out.println("Field: " + field.getName());
            System.out.println("Field type: " + field.getType());
            RandomInt annotation = field.getAnnotation(RandomInt.class);
            System.out.println("Is null: " + (annotation == null));

            if (field.getType() == int.class) {
                if (annotation != null) {
                    int number = new Random()
                            .nextInt(annotation.max() - annotation.min() + 1)
                            + annotation.min();
                    System.out.println("Number: " + number);

                    // field - number
                    // first parameter - object
                    // second parameter - value
                    field.setAccessible(true);
                    field.set(object, number);
                }
            } else {
                System.out.println("This type is not integer");
            }

            System.out.println();

        }
    }

}
